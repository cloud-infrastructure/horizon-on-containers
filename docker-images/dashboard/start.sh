#!/bin/sh

set -x

cp /opt/horizon-configmap/local_settings /opt/horizon/openstack_dashboard/local/local_settings.py
cp /opt/horizon-configmap/_91_manila_shares.py /opt/horizon/openstack_dashboard/local/local_settings/_91_manila_shares.py
cp /opt/horizon-configmap/*_policy.json /etc/openstack-dashboard/
cp /opt/horizon-configmap/httpd.conf /etc/httpd/conf/httpd.conf
mkdir -p /etc/httpd/conf.d && cp /opt/horizon-configmap/*_vhost.conf /etc/httpd/conf.d/

mkdir -p /var/log/horizon && touch /var/log/horizon/horizon.log
chown -R apache:apache /var/log/horizon

python /opt/horizon/manage.py collectstatic --noinput --clear
python /opt/horizon/manage.py compress --force

httpd -DFOREGROUND
