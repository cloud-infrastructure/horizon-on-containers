FROM cern/cc7-base
MAINTAINER Mateusz Kowalski <mateusz.kowalski@cern.ch>

ARG RELEASE=queens

# Obligatory patch for yum inside Docker
RUN yum install -y yum-plugin-ovl

# Copy CERN-rebuilds and Openstack upstream repositories
ADD yum-repos /etc/yum.repos.d/
RUN sed -i s/protect=1/protect=0/g /etc/yum.repos.d/*

RUN yum install --setopt=tsflags=nodocs -y \
    vim \
    git \
    python \
    python2-devel \
    python2-pip \
    gcc \
    openssl-devel \
    openldap-devel \
    pytz \

RUN yum install --setopt=tsflags=nodocs -y \
    httpd \
    mod_ssl \
    mod_wsgi

# Install django-redis-sentinel-redux from source
RUN cd /opt && git clone https://gitlab.cern.ch/cloud-infrastructure/django-redis-sentinel-redux.git
RUN cd /opt/django-redis-sentinel-redux && pip install -e .

# Install horizon from source
RUN cd /opt && git clone -b cern/${RELEASE} https://gitlab.cern.ch/cloud-infrastructure/horizon.git
RUN cd /opt/horizon && pip install -c https://git.openstack.org/cgit/openstack/requirements/plain/upper-constraints.txt?h=stable/${RELEASE} -r requirements.txt -e .
RUN rm -rf /etc/httpd/conf.d/

# Install manila-ui from source
RUN cd /opt && git clone -b cern/${RELEASE} https://gitlab.cern.ch/cloud-infrastructure/manila-ui.git
RUN cd /opt/manila-ui && pip install -r requirements.txt -e .
RUN cp /opt/manila-ui/manila_ui/local/enabled/_*_manila_*.py /opt/horizon/openstack_dashboard/local/enabled && \
    cp /opt/manila-ui/manila_ui/local/local_settings.d/_90_manila_*.py /opt/horizon/openstack_dashboard/local/local_settings.d

# Install mistral-ui from source
RUN cd /opt && git clone -b cern/${RELEASE} https://gitlab.cern.ch/cloud-infrastructure/mistral-ui.git
RUN cd /opt/mistral-ui && pip install -r requirements.txt -e .
RUN cp /opt/mistral-ui/mistraldashboard/enabled/_50_mistral.py /opt/horizon/openstack_dashboard/local/enabled/_50_mistral.py

# Install magnum-ui from source
#RUN cd /opt && git clone -b cern/${RELEASE} https://gitlab.cern.ch/cloud-infrastructure/magnum-ui.git
RUN cd /opt && git clone -b stable/${RELEASE} https://github.com/openstack/magnum-ui.git
RUN cd /opt/magnum-ui && pip install -r requirements.txt -e .
RUN cp /opt/magnum-ui/magnum_ui/enabled/_1370_project_container_infra_panel_group.py /opt/horizon/openstack_dashboard/local/enabled && \
    cp /opt/magnum-ui/magnum_ui/enabled/_1371_project_container_infra_clusters_panel.py /opt/horizon/openstack_dashboard/local/enabled && \
    cp /opt/magnum-ui/magnum_ui/enabled/_1372_project_container_infra_cluster_templates_panel.py /opt/horizon/openstack_dashboard/local/enabled

# Install cci-tools from source
RUN cd /opt && git clone https://gitlab.cern.ch/cloud-infrastructure/cci-tools.git
RUN cd /opt/cci-tools && pip install -r requirements.txt -e .

# Install hzrequestspanel from source
RUN cd /opt && git clone https://gitlab.cern.ch/cloud-infrastructure/hzrequestspanel.git
RUN cd /opt/hzrequestspanel && pip install -e .
RUN cp /opt/hzrequestspanel/enabled/_1021_project_hzrequests_panel.py /opt/horizon/openstack_dashboard/local/enabled/_1021_project_hzrequests_panel.py && \
    cp /opt/hzrequestspanel/enabled/_6868_project_remove_overview_panel.py /opt/horizon/openstack_dashboard/local/enabled/_6868_project_remove_overview_panel.py

# Install roboto-fontface from source
RUN cd /opt && git clone https://gitlab.cern.ch/cloud-infrastructure/xstatic-roboto-fontface.git
RUN cd /opt/xstatic-roboto-fontface && pip install -e .

# Collectstatic, compress and start httpd
COPY start.sh /
ENTRYPOINT ./start.sh
