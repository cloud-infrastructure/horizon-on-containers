OPENSTACK_MANILA_FEATURES = {
    'enable_replication': True,
    'enable_migration': True,
    'enable_public_share_type_creation': True,
    'enable_public_shares': True,
    'enabled_share_protocols': ['CephFS'],
}
