#!/bin/sh

set -x

kubectl delete secret horizon-secret-keystore
kubectl create secret generic horizon-secret-keystore --from-file ../secrets/horizon-secret-keystore

kubectl delete secret hzrequestspanel
kubectl create secret generic hzrequestspanel --from-file ../secrets/hzrequestspanel

kubectl delete secret ssl-certs
kubectl create secret generic ssl-certs --from-file ../secrets/ssl-certs
