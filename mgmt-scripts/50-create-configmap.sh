#!/bin/sh

set -x

kubectl delete configmap horizon
kubectl create configmap horizon --from-file ../configMaps/horizon-configmap/
