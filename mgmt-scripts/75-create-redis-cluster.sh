#!/bin/sh

set -x

kubectl delete service redis-bootstrap
kubectl delete service redis-sentinel

kubectl create -f ../services/redis-bootstrap-service.yaml --validate=false
kubectl create -f ../services/redis-sentinel-service.yaml --validate=false

# Create a bootstrap setup
kubectl create -f ../deployments/redis-bootstrap-pod.yaml --validate=false

# Create a deployment for redis sentinels
kubectl delete deployment redis-sentinel
kubectl create -f ../deployments/redis-sentinel-deployment.yaml --validate=false

# Create a deployment for redis servers
kubectl delete deployment redis-master
kubectl create -f ../deployments/redis-master-deployment.yaml --validate=false

# Delete the original master pod
# kubectl delete pod redis-master
# kubectl delete service redis-bootstrap
